# Use the official nginx image as the base image
FROM nginx:latest

# Copy the index.html file to the nginx web server's default directory
COPY index.html /usr/share/nginx/html

# Expose port 80, which is the default port for nginx
EXPOSE 80

# Start the nginx web server
CMD ["nginx", "-g", "daemon off;"]